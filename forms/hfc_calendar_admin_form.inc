<?php

/**
 * @file
 * Defines the Calendar Builder admin form.
 */

/**
 * HFC Calendar admin form.
 */
function hfc_calendar_admin_form($form, &$form_state) {

  $form['hfc_calendar_builder_taxonomy_vid'] = [
    '#type' => 'select',
    '#title' => t('Taxonomy vocabulary for tags'),
    '#options' => _hfc_calendar_get_taxonomy(),
    '#default_value' => variable_get('hfc_calendar_builder_taxonomy_vid', NULL),
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}

/**
 * Builds an options list of taxonomy vocabularies.
 */
function _hfc_calendar_get_taxonomy() {
  $vocabularies = taxonomy_get_vocabularies();
  $output = [];
  foreach ($vocabularies as $item) {
    $output[$item->vid] = $item->name;
  }
  return $output;
}
