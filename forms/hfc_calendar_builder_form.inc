<?php

/**
 * @file
 * Contains the Academic Calendar Builder Form.
 */

/**
 * Academic Calendar Builder Form.
 */
function hfc_calendar_builder_form($form, &$form_state, $entity) {

  drupal_set_title(t(
    '<em>Edit Calendar Builder</em> for @term',
    ['@term' => $entity->target_term]
  ), PASS_THROUGH);

  $builder = HfcCalendarBuilderService::create();

  $is_locked = !user_access('administer calendar builder');

  // Always include this.
  $form['entity'] = [
    '#type' => 'value',
    '#value' => $entity,
  ];

  if (is_array($entity->data)) {

    $form['grid_items'] = [
      '#theme' => 'hfc_calendar_builder',
      '#tree' => TRUE,
    ];

    foreach ($entity->data as $key => $item) {

      if (isset($item['nid'])) {
        $form['grid_items'][$key]['nid'] = [
          '#type' => 'value',
          '#value' => $item['nid'],
        ];
        $row_disabled = TRUE;
      }
      else {
        $row_disabled = FALSE;
      }

      $form['grid_items'][$key] = [

        'event_name' => [
          '#type' => 'textfield',
          '#size' => 50,
          '#default_value' => $item['event_name'],
          '#disabled' => $is_locked || $row_disabled,
        ],

        'start_date' => [
          '#type' => 'date_popup',
          '#title' => t('Start Date'),
          '#date_format' => 'm/d/Y',
          '#date_label_position' => 'none',
          '#date_year_range' => '0:+2',
          '#date_timezone' => date_default_timezone(),
          '#default_value' => !empty($item['start_date']) ? $item['start_date'] : 0,
        ],

        'end_date' => [
          '#type' => 'date_popup',
          '#title' => t('End Date'),
          '#date_format' => 'm/d/Y',
          '#date_label_position' => 'none',
          '#date_year_range' => '0:+2',
          '#date_timezone' => date_default_timezone(),
          '#default_value' => !empty($item['end_date']) ? $item['end_date'] : 0,
        ],

        'body' => [
          '#type' => 'textarea',
          '#default_value' => $item['body'] ?? NULL,
          '#disabled' => $is_locked || $row_disabled,
        ],

        'tags' => [
          '#type' => 'checkboxes',
          '#options' => $builder->taxonomyTerms(),
          '#default_value' => $item['tags'],
          '#disabled' => $is_locked || $row_disabled,
        ],

        // Not editable here, but should be copied
        // from the source item.
        'important' => [
          '#type' => 'value',
          '#value' => $item['important'] ?? 0,
        ],

        // Not editable here, but must be copied
        // to avoid breakage.
        'nid' => [
          '#type' => 'value',
          '#value' => $item['nid'] ?? NULL,
        ],

        '#ready' => empty($item['nid']) && !empty($item['start_date']),
        '#complete' => !empty($item['nid']),
        '#disabled' => $row_disabled,
      ];

      $form['revision_log'] = [
        '#type' => 'textarea',
        '#title' => t('Revision log message'),
        '#description' => t('Provide an explanation of the changes you are making. This will help other authors understand your motivations.'),
        '#rows' => 4,
        '#required' => TRUE,
      ];
    }
  }

  $form['submit'] = [
    '#tree' => TRUE,
    'save' => ['#type' => 'submit', '#value' => t('Save')],
  ];

  return $form;
}

/**
 * Academic Calendar Builder form validation.
 */
function hfc_calendar_builder_form_validate($form, &$form_state) {

  $grid_values = &$form_state['values']['grid_items'];
  foreach ($grid_values as $key => $values) {
    if (!empty($values['end_date']) && (strtotime($values['start_date']) >= strtotime($values['end_date']))) {
      form_set_error(
        "grid_items][$key][end_date",
        t(
          'Event start date must be before end date for %t.',
          ['%t' => $values['event_name']]
        )
      );
    }
  }
}

/**
 * Academic Calendar Builder Form submit handler.
 */
function hfc_calendar_builder_form_submit($form, &$form_state) {

  $items = $form_state['values']['grid_items'];
  $entity = $form_state['values']['entity'];

  $entity->data = $items;
  $entity->is_new_revision = TRUE;
  $entity->default_revision = TRUE;
  $entity->log = $form_state['values']['revision_log'];
  $entity->save();
  $form_state['redirect'] = $entity->path();
}
