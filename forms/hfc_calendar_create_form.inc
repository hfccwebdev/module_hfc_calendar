<?php

/**
 * @file
 * Contains the hfc_calendar creation form.
 */

/**
 * New semster creation form.
 */
function hfc_calendar_create_form($form, &$form_state) {

  drupal_set_title(t('<em>Add Calendar Builder</em>'), PASS_THROUGH);

  $form['source_term'] = [
    '#type' => 'select',
    '#title' => t('Source Term'),
    '#options' => _hfc_calendar_get_source_terms(),
    '#default_value' => !empty($form_state['values']['source_term']) ? $form_state['values']['source_term'] : NULL,
    '#required' => TRUE,
  ];

  $form['target_term'] = [
    '#type' => 'select',
    '#title' => t('Target Term'),
    '#options' => _hfc_calendar_get_target_terms(),
    '#default_value' => !empty($form_state['values']['target_term']) ? $form_state['values']['target_term'] : NULL,
    '#required' => TRUE,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Load Data'),
  ];

  return $form;
}

/**
 * New semster creation form validation.
 */
function hfc_calendar_create_form_validate($form, &$form_state) {
  if ($form_state['values']['source_term'] == $form_state['values']['target_term']) {
    form_set_error('target_term', t('Source and target terms should be different.'));
  }
}

/**
 * New semster creation form submit handler.
 */
function hfc_calendar_create_form_submit($form, &$form_state) {

  global $user;

  $source_term = $form_state['values']['source_term'];
  $target_term = $form_state['values']['target_term'];

  $data = HfcCalendarBuilderService::create()->getSourceTermContent($source_term, $target_term);

  $entity = entity_create('calendar_builder', [
    'target_term' => $target_term,
    'data' => $data,
    'uid' => $user->uid,
    'created' => REQUEST_TIME,
    'changed' => REQUEST_TIME,
    'log' => t('Create new builder from @tid', ['@tid' => $source_term]),
    'is_new_revision' => TRUE,
    'default_revision' => TRUE,
  ]);

  $entity->save();
  $form_state['redirect'] = $entity->path();
}

/**
 * Get a list of existing source terms from content.
 *
 * @return array
 *   An array of available source terms.
 */
function _hfc_calendar_get_source_terms() {

  $start_date = REQUEST_TIME - 86400 * 730;
  $term_opts = WebServicesClient::getTermsOpts(['start_date' => $start_date]);

  $existing = db_query("
    SELECT field_news_academic_term_value FROM {field_data_field_news_academic_term}
    WHERE field_news_academic_term_value IS NOT NULL GROUP BY field_news_academic_term_value
  ")->fetchCol();

  return array_intersect_key($term_opts, array_flip($existing));
}

/**
 * Get a list of allowable target terms from content.
 *
 * @return array
 *   An array of allowable target terms.
 */
function _hfc_calendar_get_target_terms() {

  $term_opts = WebServicesClient::getTermsOpts(['start_date' => REQUEST_TIME]);
  $existing = db_query("SELECT target_term FROM {hfc_calendar_builder_data}")->fetchCol();

  $result = [];
  foreach ($term_opts as $key => $value) {
    if (!isset(array_flip($existing)[$key])) {
      $result[$key] = $value;
    }
  }
  return $result;
}
