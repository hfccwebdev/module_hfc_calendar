<?php

/**
 * @file
 * Contains the Academic Calendar Import Form.
 */

/**
 * Academic Calendar Import Form.
 */
function hfc_calendar_import_form($form, &$form_state, $entity) {

  drupal_set_title(t(
    '<em>Calendar Builder Import</em> for @term',
    ['@term' => $entity->target_term]
  ), PASS_THROUGH);

  // Always include this.
  $form['entity'] = [
    '#type' => 'value',
    '#value' => $entity,
  ];

  $builder = HfcCalendarBuilderService::create();

  $content = $builder->findExistingContent($entity);
  $options = _hfc_calendar_import_options($entity->data);

  if (!empty($content) && !empty($options)) {

    $form['content'] = [
      '#type' => 'value',
      '#value' => $content,
    ];

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => t('<h2>Existing content found</h2>'),
    ];

    $form['items'] = [
      '#tree' => TRUE,
    ];

    foreach ($content as $nid => $row) {

      $form['items'][$nid]['preview'] = [
        '#type' => 'markup',
        '#prefix' => '<div class="calendar-import-preview">',
        '#markup' => t('<a href="/@path" target="_blank">@title</a> (@start)', [
          '@path' => "node/$nid",
          '@title' => $row['event_name'],
          '@start' => $row['start_date'],
        ]),
        '#suffix' => '</div>',
      ];

      $form['items'][$nid]['selection'] = [
        '#type' => 'select',
        '#options' => ['' => ' - select one - '] + $options,
      ];
    }
  }
  else {
    $form['no_content'] = [
      '#type' => 'markup',
      '#markup' => t('<p><strong>No content found to import.</strong></p>'),
    ];
  }

  $form['submit'] = [
    '#tree' => TRUE,
    'import' => [
      '#type' => 'submit',
      '#value' => t('Import'),
      '#disabled' => empty($form['items']),
    ],
  ];

  return $form;
}

/**
 * The Calendar Builder Publish form submit.
 */
function hfc_calendar_import_form_submit($form, &$form_state) {

  $entity = $form_state['values']['entity'];
  $items = $form_state['values']['items'];
  $content = $form_state['values']['content'];
  foreach ($items as $key => $item) {
    if (is_numeric($item['selection'])) {
      $entity->data[$item['selection']] = $content[$key];
    }
  }
  $entity->is_new_revision = TRUE;
  $entity->default_revision = TRUE;
  $entity->log = 'Import to link existing content to builder.';
  $entity->save();
  $form_state['redirect'] = $entity->path();
}

/**
 * Build a list of select options to for matching import content.
 *
 * @param array $data
 *   The existing semester data.
 *
 * @return array
 *   An array of select list options.
 */
function _hfc_calendar_import_options(array $data): array {
  $output = [];
  foreach ($data as $key => $row) {
    if (empty($row['nid'])) {
      $output[$key] = t('@title (@start)', [
        '@title' => $row['event_name'],
        '@start' => !empty($row['start_date']) ? $row['start_date'] : 'no date',
      ]);
    }
  }
  return $output;
}
