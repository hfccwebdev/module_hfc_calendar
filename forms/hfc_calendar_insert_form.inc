<?php

/**
 * @file
 * Contains the Academic Calendar Insert Form.
 */

/**
 * Academic Calendar Insert Form.
 */
function hfc_calendar_insert_form($form, &$form_state, $entity) {

  drupal_set_title(t(
    '<em>Insert Calendar Builder Entry</em> for @term',
    ['@term' => $entity->target_term]
  ), PASS_THROUGH);

  $builder = HfcCalendarBuilderService::create();

  // Always include this.
  $form['entity'] = [
    '#type' => 'value',
    '#value' => $entity,
  ];

  $form['event_name'] = [
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => 50,
    '#required' => TRUE,
  ];

  $form['start_date'] = [
    '#type' => 'date_popup',
    '#title' => t('Start Date'),
    '#date_format' => 'm/d/Y',
    '#date_label_position' => 'none',
    '#date_year_range' => '0:+2',
    '#date_timezone' => date_default_timezone(),
  ];

  $form['end_date'] = [
    '#type' => 'date_popup',
    '#title' => t('End Date'),
    '#date_format' => 'm/d/Y',
    '#date_label_position' => 'none',
    '#date_year_range' => '0:+2',
    '#date_timezone' => date_default_timezone(),
  ];

  $form['body'] = [
    '#type' => 'textarea',
    '#title' => t('Description'),
  ];

  $form['tags'] = [
    '#type' => 'checkboxes',
    '#title' => t('Tags'),
    '#options' => $builder->taxonomyTerms(),
  ];

  $form['submit'] = [
    '#tree' => TRUE,
    'save' => ['#type' => 'submit', '#value' => t('Add entry')],
  ];

  return $form;
}

/**
 * Academic Calendar Insert Form Validate.
 */
function hfc_calendar_insert_form_validate($form, &$form_state) {

  $values = $form_state['values'];
  if (!empty($values['end_date']) && (strtotime($values['start_date']) >= strtotime($values['end_date']))) {
    form_set_error(
      "end_date",
      t(
        'Event start date must be before end date for %t.',
        ['%t' => $values['event_name']]
      )
    );
  }
}

/**
 * Academic Calendar Insert Form Submit.
 */
function hfc_calendar_insert_form_submit($form, &$form_state) {

  $items = $form_state['values'];
  $entity = $form_state['values']['entity'];

  $entity->data[] = [
    'event_name' => $items['event_name'],
    'start_date' => $items['start_date'],
    'end_date' => $items['end_date'],
    'body' => $items['body'],
    'tags' => $items['tags'],
    'important' => 0,
    'nid' => NULL,
  ];

  $entity->is_new_revision = TRUE;
  $entity->default_revision = TRUE;
  $entity->log = t('Insert item: @title', ['@title' => $items['event_name']]);

  $entity->save();
  $form_state['redirect'] = $entity->path();
}
