<?php

/**
 * @file
 * Contains the Calendar Builder Publish form.
 */

/**
 * The Calendar Builder Publish form.
 */
function hfc_calendar_publish_form($form, &$form_state, $entity) {

  drupal_set_title(t(
    '<em>Publish Calendar Builder</em> for @term',
    ['@term' => $entity->target_term]
  ), PASS_THROUGH);

  $controller = entity_get_controller($entity->entityType());

  // Always include this.
  $form['entity'] = [
    '#type' => 'value',
    '#value' => $entity,
  ];

  if (is_array($entity->data)) {

    $rows = [];
    foreach ($entity->data as $key => $item) {
      if (empty($item['nid']) and !empty($item['start_date'])) {
        $rows[$key] = $controller->formatPreviewRow($item);
      }
    }

    $form['ready'] = [
      '#type' => 'markup',
      '#markup' => t('<h2>The following items are ready to publish.</h2>'),
    ];

    $form['items'] = [
      '#theme' => 'table',
      '#header' => [
        'event_name' => t('Title'),
        'start_date' => t('Start/End Date'),
        'body' => t('Description'),
        'tags' => t('Tags'),
      ],
      '#rows' => $rows,
      '#empty' => t('There are no items ready to publish.'),
    ];
  }

  $form['submit'] = [
    '#tree' => TRUE,
    'publish' => [
      '#type' => 'submit',
      '#value' => t('Publish'),
      '#disabled' => empty($form['items']['#rows']),
    ],
  ];

  return $form;
}

/**
 * The Calendar Builder Publish form submit.
 */
function hfc_calendar_publish_form_submit($form, &$form_state) {

  $entity = $form_state['values']['entity'];
  $controller = entity_get_controller($entity->entityType());
  $controller->publish($entity);
  $form_state['redirect'] = $entity->path();
}
