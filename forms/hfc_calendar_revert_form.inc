<?php

/**
 * @file
 * Contains the Academic Calendar Revision Revert Form.
 */

/**
 * Academic Calendar Revision Revert Form.
 */
function hfc_calendar_revert_form($form, &$form_state, $revision) {

  $form['#calendar_revision'] = $revision;
  return confirm_form(
    $form,
    t(
      'Are you sure you want to revert to the revision from %revision-date?',
      ['%revision-date' => format_date($revision->revision_time)]
    ),
    "node/{$revision->id}/revisions",
    '',
    t('Revert'),
    t('Cancel')
  );
}

/**
 * Academic Calendar Revision Revert Form Submit.
 */
function hfc_calendar_revert_form_submit($form, &$form_state) {

  $revision = $form['#calendar_revision'];

  $revision->is_new_revision = TRUE;
  $revision->default_revision = TRUE;
  $revision->log = t(
    'Revert to revision from %revision-date.',
    ['%revision-date' => format_date($revision->revision_time)]
  );
  $revision->save();
  watchdog(
    'hfc_calendar',
    'Reverted %title to revision %revision.',
    ['%title' => $revision->label(), '%revision' => $revision->vid]
  );

  drupal_set_message(t(
    '%title has been reverted back to the revision from %revision-date.',
    [
      '%title' => $revision->label(),
      '%revision-date' => format_date($revision->revision_time),
    ]
  ));

  $form_state['redirect'] = $revision->path();
}
