<?php

/**
 * Page display controller for HFC Calendar Builder.
 */
class HfcCalendarBuilderPageController {

  /**
   * Displays the overview page.
   */
  public static function overview() {

    $ids = db_query("SELECT id FROM {hfc_calendar_builder_data} ORDER BY id DESC LIMIT 12")->fetchCol();
    $entities = entity_load('calendar_builder', $ids);

    $rows = array_map(function ($entity) {
      return [
        $entity->shortLink(),
        $entity->uid ? (user_load($entity->uid)->name ?? $entity->uid) : t('unknown'),
        $entity->changed ? format_interval(REQUEST_TIME - $entity->changed, 1) : NULL,
        $entity->editLink(),
      ];
    }, $entities);

    return [
      '#theme' => 'table',
      '#header' => [
        t('Semester'),
        t('Created by'),
        t('Last Update'),
        t('Operations'),
      ],
      '#rows' => $rows,
    ];
  }

  /**
   * Displays a single entity.
   */
  public static function view($entity) {
    return $entity->view('full', NULL, TRUE);
  }

  /**
   * Displays a single entity revision.
   */
  public static function viewRevision($revision) {
    drupal_set_title(
      t('Revision %vid for %title', [
        '%vid' => $revision->vid,
        '%title' => $revision->label(),
      ]),
      PASS_THROUGH
    );
    return $revision->view('full', NULL, TRUE);
  }

  /**
   * Displays an entity revision list.
   */
  public static function revisions($entity) {
    drupal_set_title(
      t('Revisions for %title', ['%title' => $entity->label()]),
      PASS_THROUGH
    );

    $rows = array_map(function ($revision) use ($entity) {
      $label = !empty($revision->revision_time)
        ? format_date($revision->revision_time)
        : t('unknown');

      $link = l($label, "{$entity->path()}/revisions/{$revision->vid}/view");

      $author = !empty($revision->revision_author)
        ? l($revision->revision_author, "user/{$revision->revision_uid}")
        : 'unknown';

      $view = "{$link} by {$author}";
      if (!empty($revision->log)) {
        $view .= "<p>{$revision->log}</p>";
      }

      $revert = user_access('administer calendar builder') && ($revision->vid !== $entity->vid)
        ? l(t('Revert'), "{$entity->path()}/revisions/{$revision->vid}/revert")
        : NULL;

      return [
        'revision' => $view,
        'revert' => $revert,
      ];
    }, $entity->revisions());

    return [
      [
        '#theme' => 'table',
        '#header' => [
          t('Revision'),
          t('Operations'),
        ],
        '#rows' => $rows,
        '#empty' => t('Revision info could not be loaded.'),
      ],
    ];
  }

}
