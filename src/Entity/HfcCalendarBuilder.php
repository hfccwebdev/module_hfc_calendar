<?php

/**
 * Defines the HFC Calendar Builder entity.
 */
class HfcCalendarBuilder extends Entity {

  /**
   * Taxononomy term values to display.
   *
   * @var array
   */
  private $tags = [];

  /**
   * {@inheritdoc}
   */
  protected function defaultLabel() {
    return t('Academic Calendar Builder for @term', ['@term' => $this->target_term]);
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultUri() {
    $term = str_replace('/', '', $this->target_term);
    return ['path' => "admin/content/calendar/{$term}"];
  }

  /**
   * Gets the identifier using Drupal 8+ syntax.
   *
   * @return int
   *   The Entity ID.
   */
  public function id(): int {
    return $this->identifier();
  }

  /**
   * Get the link path.
   *
   * @return string
   *   The path to the entity display.
   */
  public function path() {
    return $this->uri()['path'];
  }

  /**
   * Get the entity link.
   *
   * @return string
   *   An HTML link to the entity with full title.
   */
  public function toLink(): string {
    return l($this->label(), $this->uri()['path']);
  }

  /**
   * Get the short link with just the term id for text.
   *
   * @return string
   *   An HTML link to the entity with short title.
   */
  public function shortLink(): string {
    return l($this->target_term, $this->uri()['path']);
  }

  /**
   * Get the link to edit this entity.
   *
   * @return string
   *   An HTML link to the edit form for the entity.
   */
  public function editLink(): string {
    return entity_access('update', $this->entityType)
      ? l(t('edit'), $this->uri()['path'] . '/edit')
      : '';
  }

  /**
   * Get list of revisions.
   */
  public function revisions() {
    return entity_get_controller($this->entityType)->getRevisions($this);
  }

}
