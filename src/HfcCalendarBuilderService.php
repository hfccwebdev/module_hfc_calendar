<?php

/**
 * Defines the HFC Academic Calendar Builder Service.
 */
class HfcCalendarBuilderService {

  /**
   * Create an instance of this class.
   */
  public static function create() {
    return new static();
  }

  /**
   * Gets a list of HANK Academic Terms.
   *
   * @return array
   *   An array of academic term ids and names.
   */
  public function academicTerms(): array {
    $terms = &drupal_static(__FUNCTION__);
    if (!isset($terms)) {
      $term_date = REQUEST_TIME - 86400 * 545;
      $terms = WebServicesClient::getTermsOpts(['start_date' => $term_date]);
    }
    return $terms;
  }

  /**
   * Gets all available taxonomy terms from vocabulary.
   *
   * @return array
   *   An array of taxonomy tags available to the builder.
   */
  public function taxonomyTerms(): array {
    $terms = &drupal_static(__FUNCTION__);
    if (!isset($terms)) {
      $vid = variable_get('hfc_calendar_builder_taxonomy_vid', 1);
      $query = db_select("taxonomy_term_data", "t");

      $query->join("taxonomy_term_hierarchy", "h", "h.tid = t.tid");
      $query->join("taxonomy_term_data", "p", "p.tid = h.parent");
      $query->leftJoin("field_data_field_calendar_builder", "c", "c.entity_id = t.tid AND c.entity_type='taxonomy_term'");

      $query->condition("t.vid", $vid);
      $query->condition("c.field_calendar_builder_value", 1);

      $query->fields("t", ["tid", "name"]);
      $query->orderBy("p.weight");
      $query->orderBy("t.weight");
      $terms = $query->execute()->fetchAllKeyed();
    }
    return $terms;
  }

  /**
   * Retrieve content from the source term.
   *
   * @param string $source_term
   *   The Term ID for the source term.
   * @param string $target_term
   *   The Term ID for the new term.
   *
   * @return array
   *   An array of data for the new builder entity.
   */
  public function getSourceTermContent($source_term, $target_term) {

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'news')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_news_academic_term', 'value', $source_term)
      ->fieldOrderBy('field_news_event_date', 'value');
    $results = $query->execute();

    $nids = array_map(function ($node) {
      return $node->nid;
    }, $results['node']);

    $nodes = node_load_multiple($nids);

    $output = [];
    foreach ($nodes as $node) {
      $output[] = [
        'event_name' => $this->stringReplace($node->title, $source_term, $target_term),
        'start_date' => NULL,
        'end_date' => NULL,
        'important' => !empty($node->field_important_event) ? 1 : 0,
        'body' => (
          !empty($node->body)
            ? $this->stringReplace($node->body[LANGUAGE_NONE][0]['value'], $source_term, $target_term)
            : NULL
        ),
        'tags' => (
          !empty($node->field_news_tags)
            ? $this->addSourceTags($node->field_news_tags[LANGUAGE_NONE])
            : []
        ),
      ];
    }

    return $output;
  }

  /**
   * Retrieve existing content from the current term.
   *
   * @param object $entity
   *   The current term object.
   *
   * @return array
   *   An array of possible data updates.
   */
  public function findExistingContent(object $entity): array {

    $output = [];

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'news')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_news_academic_term', 'value', $entity->target_term)
      ->fieldOrderBy('field_news_event_date', 'value');

    if ($results = $query->execute()) {

      $matches = [];
      foreach ($entity->data as $row) {
        if (!empty($row['nid'])) {
          $matches[$row['nid']] = $row['nid'];
        }
      }

      $nids = [];

      foreach ($results['node'] as $row) {
        if (!isset($matches[$row->nid])) {
          $nids[$row->nid] = $row->nid;
        }
      }

      $nodes = node_load_multiple($nids);

      foreach ($nodes as $node) {
        $start_date = !empty($node->field_news_event_date[LANGUAGE_NONE][0]['value'])
          ? format_date($node->field_news_event_date[LANGUAGE_NONE][0]['value'], 'custom', 'Y-m-d')
          : NULL;
        $end_date = !empty($node->field_news_event_date[LANGUAGE_NONE][0]['value2'])
          ? format_date($node->field_news_event_date[LANGUAGE_NONE][0]['value2'], 'custom', 'Y-m-d')
          : NULL;
        if ($end_date == $start_date) {
          $end_date = NULL;
        }

        $output[$node->nid] = [
          'nid' => $node->nid,
          'event_name' => $node->title,
          'start_date' => $start_date,
          'end_date' => $end_date,
          'important' => !empty($node->field_important_event) ? 1 : 0,
          'body' => (
            !empty($node->body)
              ? $node->body[LANGUAGE_NONE][0]['value']
              : NULL
          ),
          'tags' => (
            !empty($node->field_news_tags)
              ? $this->addSourceTags($node->field_news_tags[LANGUAGE_NONE])
              : []
          ),
        ];
      }
    }

    return $output;
  }

  /**
   * Replace strings in content.
   */
  private function stringReplace($text, $source_term, $target_term) {
    $text = trim($text);
    $source_term_name = $this->academicTerms()[$source_term];
    $target_term_name = $this->academicTerms()[$target_term];
    return str_replace($source_term_name, $target_term_name, $text);
  }

  /**
   * Add taxonomy terms to output data.
   */
  private function addSourceTags($items) {
    $output = [];
    foreach ($items as $item) {
      $tid = $item['tid'];
      $output[$tid] = $tid;
    }
    return array_intersect_key($output, $this->taxonomyTerms());
  }

}
