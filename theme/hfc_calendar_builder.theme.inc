<?php

/**
 * @file
 * Includes theme functions for hfc_calendar_builder.
 *
 * @see hfc_calendar.module
 */

/**
 * Theme the editing form as a table.
 */
function theme_hfc_calendar_builder($variables) {

  $element = $variables['element'];

  $header = [
    'event_name' => t('Title'),
    'start_date' => t('Start Date'),
    'end_date' => t('End Date'),
    'body' => t('Description'),
    'tags' => t('Tags'),
  ];

  $rows = [];
  foreach (element_children($element) as $key) {
    $row = [];
    $row['data'] = [];
    foreach (array_keys($header) as $fieldname) {
      $row['data'][] = drupal_render($element[$key][$fieldname]);
    }

    if ($element[$key]['#complete']) {
      $row['class'][] = 'complete';
    }
    if ($element[$key]['#ready']) {
      $row['class'][] = 'ready';
    }

    $rows[] = $row;
  }

  // This is a theme function, not preprocess.
  // We MUST call theme() here in order to return a table!
  return theme('table', [
    'header' => $header,
    'rows' => $rows,
    'attributes' => ['id' => 'calendar-builder-table'],
  ]);
}
